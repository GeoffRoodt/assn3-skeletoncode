﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using SetupSQLite;
using SQLite;
using Xamarin.Forms;

namespace ASSN3
{
	public class WebURL
	{
		public int id { get; set; }
		public string url { get; set; }
		public string image { get; set; }
		public string title { get; set; }
	}

	public partial class ASSN3Page : ContentPage
	{
		public ASSN3Page()
		{
			InitializeComponent();
		}

		async void ReloadPicker()
		{
			if (ShowURlS.Items.Count != 0)
			{
				ShowURlS.Items.Clear();
			}

			foreach (var x in urls)
			{
				ShowURlS.Items.Add($"{x.title}");
			}
		}

		protected override void OnAppearing()
		{
			ReloadPicker();

			base.OnAppearing();
		}

		async void OpenWebView(object sender, System.EventArgs e)
		{
			await Navigation.PushModalAsync(new WebViewShow(passOnUrl));
		}

		async void AddURLAction(object sender, System.EventArgs e)
		{
			await Navigation.PushModalAsync(new AddURLPage());
		}

		async void UpdateURLAction(object sender, System.EventArgs e)
		{
			await Navigation.PushModalAsync(new EditURLPage());
		}

		async void REMOVEURL(object sender, System.EventArgs e)
		{

		}
	}
}